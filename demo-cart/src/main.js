import Vue from 'vue'
import App from './App.vue'
import axios from 'axios'
// 导入 bootstrap 样式表
import 'bootstrap/dist/css/bootstrap.min.css'

Vue.config.productionTip = false
// 把 axios 挂载到 Vue.prototype 上，供每个 .vue 组件的实例直接使用
Vue.prototype.$http = axios

new Vue({
  render: h => h(App)
}).$mount('#app')
